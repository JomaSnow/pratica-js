let buttonRef = document.querySelector("button");
let headers = document.querySelectorAll("#table-colunas>th");
let resultsRow = document.querySelector("tr#tabela-total");

buttonRef.onclick = function () {
    document.querySelector("div.main").classList.toggle("bg-dark");
    document.querySelector("h2").classList.toggle("text-dark");

    if(resultsRow.childNodes[1].classList.contains("text-dark")){
        resultsRow.childNodes[1].classList.toggle("text-dark", false);
        resultsRow.childNodes[3].classList.toggle("text-dark", false);
        resultsRow.childNodes[1].classList.toggle("text-light", true);
        resultsRow.childNodes[3].classList.toggle("text-light", true);
    }else{
        resultsRow.childNodes[1].classList.toggle("text-dark", true);
        resultsRow.childNodes[3].classList.toggle("text-dark", true);
        resultsRow.childNodes[1].classList.toggle("text-light", false);
        resultsRow.childNodes[3].classList.toggle("text-light", false);
    }

    if(buttonRef.classList.contains("btn-dark")){
        buttonRef.classList.toggle("btn-dark", false);
        buttonRef.classList.toggle("btn-light", true);
    }else{
        buttonRef.classList.toggle("btn-dark", true);
        buttonRef.classList.toggle("btn-light", false);
    }

    for(let i=0; i<headers.length; i++){
        if(headers[i].classList.contains("bg-light")){
            headers[i].classList.toggle("bg-light", false);
            headers[i].classList.toggle("text-dark", false);
            headers[i].classList.toggle("text-light", true);
            headers[i].classList.toggle("bg-dark", true);
        }else{
            headers[i].classList.toggle("bg-light", true);
            headers[i].classList.toggle("text-dark", true);
            headers[i].classList.toggle("text-light", false);
            headers[i].classList.toggle("bg-dark", false);
        }
    }
}