let tableRows = document.querySelectorAll("tr");
let valuesSum = 0;

for (let i = 1; i < tableRows.length-1; i++) {
    if(tableRows[i].childNodes[7].textContent>0){
        tableRows[i].classList.add("table-success");
    }else{
        tableRows[i].classList.add("table-danger");
    }
    valuesSum+= Number(tableRows[i].childNodes[7].textContent);
}

tableRows[8].childNodes[3].innerHTML = parseFloat(valuesSum).toFixed(2);